# Statistical Example Model
This model provides a starting point for developing a metric driven approach for the competition.

## Initializing the Environment
  1. Create a python environment
      - If using virtual enviornments: `python -m venv venv`
  2. Use pip to install the modules in the requirements.txt `pip install -r requirements.txt`

## Model Definition
This model is a lightweight example primarily to demonstrate how to properly generate the output for the competition.

The "model" in this instance is actually just a coin flip (50/50) approach to picking a winner. After generating all of the possible matchups, each matchup is decided by a programmatic coin flip.
