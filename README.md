# Felicia 2024 Ai Bracket

## Predictions
Predictions are located in the predictions.csv file. To fill in your bracket you'll need to look up the two teams in the csv:
 - $\text{is\_team\_a} = 1$, that means that team_a won that matchup.
 - $\text{is\_team\_a} = 0$, that means that team_b won that matchup.

## But my model's better, so:

![Hi Felicia](imgs/ice-cube-bye-felicia.gif)
