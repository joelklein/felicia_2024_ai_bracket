import random
import pandas as pd
import numpy as np

# Set seeds for the model to ensure reproducibility of results
seed = 22
random.seed(seed)
np.random.seed(seed)


# Initilaize runtime configuration 
curr_year = 2024 # Using 2023 as 2024 data is not available

#######################################################
# Generate all possible team pairs for the tournament
#######################################################

# Load in the tournament seeds and subset to the selected year
seed_df = pd.read_csv('data/MNCAATourneySeeds.csv')
seed_df = seed_df[seed_df['Season'] == curr_year]

team_names_df = pd.read_csv('data/MTeams.csv')
merged_df = pd.merge(seed_df, team_names_df)

# Create all team pairings
teams = merged_df['TeamName']
team_pairs = []

# For each team, we pair them up with every other team
for ix, team_a in enumerate(teams):
    for team_b in teams[ix+1:]:
        # Provide a consistent ordering to avoid duplicates
        if team_a.lower() < team_b.lower():
            team_pairs.append({
                'team_a': team_a,
                'team_b': team_b
            })
        else:
            team_pairs.append({
                'team_a': team_b,
                'team_b': team_a
            })

# Create a dataframe based on the generated pairings
team_pairs_df = pd.DataFrame(team_pairs)
print(f"Number of pairings are - {len(team_pairs)}")

###############################################################################
# Model definition:
# This model emulates a coin-flip bracket by using a random number generator to
# predict the outcome of each pairing. Essentially we are "flipping a coin" on
# each team pair to make our determination.
###############################################################################


results_lst = []
for team_pair in team_pairs:
    
    team_a = team_pair['team_a']
    team_b = team_pair['team_b']
    
    # If the "coin flip" > 0.5 we assume team a wins, otherwise team b wins
    if np.random.uniform() > 0.5:
        results_lst.append({
            'team_a': team_a,
            'team_b': team_b,
            'is_team_a_win': 1
        })
    else:
        results_lst.append({
            'team_a': team_a,
            'team_b': team_b,
            'is_team_a_win': 0
        })
        
# Lastly we save the outputs of our predictions to a predictions.csv file.
results_df = pd.DataFrame(results_lst).sort_values(['team_a', 'team_b'])
results_df.to_csv("predictions.csv", index=False)
        